<?php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController
{
    #[Route('/hihi', name:'hihi')]
    public function index()
    {
        return new \Symfony\Component\HttpFoundation\RedirectResponse('https://www.google.com/');
    }
}
